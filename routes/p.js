var express = require('express');
var p = express.Router();
const con = require('../constants');

p.get('/', (req, res) => {
	res.redirect('/portfolio');
});
p.get('/thaldrin', (req, res) => {
	res.redirect('https://gitdab.com/r/thaldrin');
});
p.get('/kaito', (req, res) => {
	res.redirect('https://github.com/codepupper/kaito');
});
p.get('/website', (req, res) => {
	res.redirect('https://gitdab.com/y/website');
});
p.get('/yiff', (req, res) => {
	res.redirect('https://github.com/codepupper/yiff');
});

module.exports = p;
