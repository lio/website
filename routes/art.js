var express = require('express');
var art = express.Router();

art.get('/', (req, res) => {
	res.status(200).jsonp({
		site: 'art',
		done: false
	});
});

module.exports = art;
