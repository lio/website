var express = require('express');
var lore = express.Router();
const con = require('../constants');

lore.get('/', (req, res) => {
	res.render('story', {
		layout: 'lore',
		name: `${con.name}`,
		host: req.hostname
	});
});

module.exports = lore;
