//! Deps
// wrwlf.co
const express = require("express");
const helmet = require("helmet");
const compression = require("compression");
const cors = require("cors");
const morgan = require("morgan");
const chalk = require("chalk");
const hbs = require("express-handlebars");
const UA = require("ua-parser-js");
const exec = require("shell-exec");
const simpleIcons = require("simple-icons");

const con = require("./constants");
let { port } = {
  port: 7777
};

const app = express();

app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultView: "default",
    helpers: {
      ifeq: function(a, b, options) {
        if (a === b) {
          return options.fn(this);
        }
      }
    }
  })
);

app.set("view engine", "hbs");
app.set("json spaces", 4);
app.use("/assets", express.static("./assets"));
app.set("view options", {
  layout: false
});
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true
  })
);
app.use(helmet());
app.use(compression());
app.use(cors());
// Logging
app.use(
  morgan((tokens, req, res) => {
    return [
      chalk.hex("#34ace0").bold(`[ ${tokens.method(req, res)} ]`),
      chalk.hex("#ffb142").bold(tokens.status(req, res)),
      chalk.hex("#ff5252").bold(req.hostname + tokens.url(req, res)),
      chalk.hex("#2ed573").bold(tokens["response-time"](req, res) + "ms"),
      chalk.hex("#f78fb3").bold("@ " + tokens.date(req, res))
    ].join(" ");
  })
);

app.use("/portfolio", require("./routes/portfolio"));
app.use("/p", require("./routes/p"));
app.use("/lore", require("./routes/lore"));
app.use("/art", require("./routes/art"));

module.exports = client => {
  app.get("/", async (req, res) => {
    res.render("index", {
      layout: "main",
      name: con.name,
      host: req.hostname,
      item: [{ name: "portfolio", source: "https://p.wrwlf.co" }],
      social: [
        {
          name: "Twitter",
          color: "1DA1F2",
          link: "twitter.com/codepupper",
          icon: simpleIcons.Twitter
        },
        {
          name: "Mastodon",
          color: "3088d4",
          link: "pounced-on.me/@code",
          icon: simpleIcons.Mastodon,
          verify: true
        },
        {
          name: "Twitter",
          color: "1DA1F2",
          link: "twitter.com/floofydev",
          icon: simpleIcons.Twitter
        },
        {
          name: "Discord",
          color: "7289da",
          link: "thaldr.in/discord",
          icon: simpleIcons.Discord
        },
        {
          name: "Twitch",
          color: "6441a4",
          link: "twitch.tv/codepupper",
          icon: simpleIcons.Twitch
        },
        {
          name: "Keybase",
          color: "33a0ff",
          link: "keybase.io/codepupper",
          icon: simpleIcons.Keybase
        },
        {
          name: "Gitdab",
          color: "faacb7",
          link: "gitdab.com/y",
          icon: simpleIcons.Git
        },
        {
          name: "Github",
          color: "333",
          link: "github.com/codepupper",
          icon: simpleIcons.GitHub
        },
        {
          name: "Gitlab",
          color: "E24329",
          link: "gitlab.com/codepupper",
          icon: simpleIcons.GitLab
        },
        {
          name: "Ko-Fi",
          color: "f16061",
          link: "ko-fi.com/codepupper",
          icon: simpleIcons["Ko-fi"]
        },
        {
          name: "Patreon",
          color: "6441a4",
          link: "patreon.com/ryden",
          icon: simpleIcons.Patreon
        }
      ]
    });
  });

  app.get("/update", async (req, res) => {
    //console.log(req.headers['user-agent']);
    if (req.headers.authorization === con.updateToken) {
      await exec("git pull")
        .then(r => {
          console.log("Pulled latest changes");
          res.status(200).jsonp({
            success: true,
            message: "Successfully pulled latest changes"
          });
          process.exit();
          //trying.edit('```fix\n' + r.stdout + '```');
        })
        .catch(error => {
          res.status(400).jsonp({
            success: false,
            message: error.message
          });
          console.error(error);
        });
    } else {
      res.status(400).jsonp({
        success: false,
        message: "You are not authorized to access this Endpoint"
      });
    }
  });

  app.listen(port /* , hostname */, () => {
    console.log(`[ Server ] Listening on ${port}`);
  });
};
